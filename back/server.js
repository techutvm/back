var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

//variable para la conexión a MLAB (a la db de Ángel)
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=50c5ea68e4b0a97d668bc84a";
var clienteMlab = requestjson.createClient(urlmovimientosMlab);

//variable para la conexión al mongo que tenemos en el contenedor local
//si este proyecto va a ir en un contenedor, no puedo usar localhost, debo usar el nombre del contenedor que tiene el mongo
var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/local";

//variable para la conexión al contenedor postgreSQL
//la URL del servidor es el nombre del contenedor
//además requiere el nombre de la base de datos
var pg = require('pg');
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad";
//Línea para cuando se llame desde docker
//var urlUsuarios = "postgres://docker:docker@postgresdb:5433/bdseguridad";
var clientePostgre = new pg.Client(urlUsuarios);
clientePostgre.connect();
// Asumo que recibo req.body = {usario:xxxx, password:yyyy}

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//GET para la db en MLAB
app.get('/movimientos', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

//GET para la db del contenedor Mongo
app.get('/movimientosMongoContainer', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url,function(err, db) {
      if (err)
      {
        console.log(err);
      }
      else {
        console.log("Connected successfully to server");
        var col = db.collection('movimientos');
        col.find({}).limit(3).toArray(function(err, docs) {
          res.send(docs);
        });
        db.close();
      }
    });
});

app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('&f={"idcliente":1, "nombre": 1, "apellidos": 1}', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/movimientos', function(req, res) {
  /* Crear movimiento en MLab */

});

//POST para la db del contenedor Mongo
app.post ('/movimientosMongoContainer', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url,function(err, db) {
      var col = db.collection('prueba');
      console.log("Traying to insert to server");
      //Comento las dos primeras formas de inserción ya que voy a hacerlo por el body
      // db.collection('movimientos').insertOne({a:1}, function(err, r){
      //   console.log(r.insertedCount + ' registros insertados');
      // });
      //
      //
      // //Insert multiple documents
      // db.collection('movimientos').insertMany([{a:2}, {a:3}], function(err, r){
      //   console.log(r.insertedCount + ' registros insertados');
      // });

      db.collection('movimientos').insertOne(req.body, function(err, r){
        console.log(r.insertedCount + ' registros insertados body');
      });

      db.close();
      res.send("ok");
    })
});

app.post ('/login', function(req, res) {
  // Crear cliente postgreSQL
  clientePostgre.connect();
  //console.log('Estoy conectado');
  //Asumo que recibo req.body = {login:xxxx, password:yyyy}
  //Hacer consulta
  //El tercer parámetro
  const query =  clientePostgre.query('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2;', [req.body.login, req.body.password], (err, result) => {
  if (err){
    console.log(err);
    res.send(err);
  }
  else {
    //Esto sólo vale para mostrar el primer registro
    //NO está contralando si no se devuelve nada o bien si se devuelve más de un registro (que habría que controlarlo)
    if (result.rows[0].count>= 1)
    {
      res.send("Login correcto");
    }
    else {
      //console.log (result.rows[0]);
      //res.send(result.rows[0]);
      res.send("Login incorrecto");
    }
  }
});
});


app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
});
